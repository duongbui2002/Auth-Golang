package main

// ? Require the packages
import (
	"AuthProject/config"
	"context"
	"fmt"
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
)

// ? Create required variables that we'll re-assign later
var (
	server      *gin.Engine
	ctx         context.Context
	mongoClient *mongo.Client
)

func init() {
	loadConfig, err := config.LoadConfig(".")
	if err != nil {
		log.Fatal("Could not load environment variables", err)
	}
	ctx = context.TODO()
	mongoconn := options.Client().ApplyURI(loadConfig.DBUri)
	mongoClient, err := mongo.Connect(ctx, mongoconn)

	if err != nil {
		panic(err)
	}

	if err := mongoClient.Ping(ctx, readpref.Primary()); err != nil {
		panic(err)
	}

	fmt.Println("MongoDB successfully connected...")

	server = gin.Default()
}

func main() {
	loadConfig, err := config.LoadConfig(".")

	if err != nil {
		log.Fatal("Could not load loadConfig", err)
	}

	defer mongoClient.Disconnect(ctx)

	if err != nil {
		panic(err)
	}

	router := server.Group("/api")
	router.GET("/healthchecker", func(ctx *gin.Context) {
		ctx.JSON(http.StatusOK, gin.H{"status": "success", "message": "hello"})
	})

	log.Fatal(server.Run(":" + loadConfig.Port))
}
